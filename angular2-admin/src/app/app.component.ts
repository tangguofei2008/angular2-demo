import { Component } from '@angular/core';
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router';
import {HTTP_PROVIDERS} from '@angular/http';

import {IndexComponent} from './components/index.component';

@Component({
    selector: 'my-app',
    directives: [RouterLink, ROUTER_DIRECTIVES, IndexComponent],
    template: `
         <index></index>
      `
})
export class AppComponent {

}


