import { NgModule, provide, ExceptionHandler } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { appRouterProviders} from './app.routes';
import { AppComponent } from './app.component';

import {LoginComponent} from './components/login.component';
import {IndexComponent} from './components/index.component';
import {AnnounceTabComponent} from './components/announce/announce-tab.component';
import {AnnounceEditComponent} from './components/announce/announce-edit.component';
import {AnnounceViewComponent} from './components/announce/announce-view.component';
import {AnnounceListComponent} from './components/announce/announce-list.component';
import {RoleListComponent} from './components/roles/role-list.component';
import {RolePrivilegesComponent} from './components/roles/role-privileges.component';
import {MemberListComponent} from './components/members/member-list.component';
import {SettingsArbitratorComponent} from './components/settings/arbitrator.component';
import {CustomExceptionHandler} from './exception/exception-handler';


@NgModule({
  imports: [
    BrowserModule,
    appRouterProviders,
    FormsModule
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    IndexComponent,
    AnnounceTabComponent,
    AnnounceEditComponent,
    AnnounceViewComponent,
    AnnounceListComponent,
    RoleListComponent,
    MemberListComponent,
    SettingsArbitratorComponent,
    RolePrivilegesComponent
  ],
  bootstrap: [ AppComponent ],
  providers: [
              {provide: ExceptionHandler, useClass: CustomExceptionHandler}
             ]
})
export class AppModule { }
