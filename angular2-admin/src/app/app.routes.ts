import { Routes, RouterModule } from '@angular/router';

import {LoginComponent} from './components/login.component';
import {IndexComponent} from './components/index.component';
import {AnnounceTabComponent} from './components/announce/announce-tab.component';
import {AnnounceEditComponent} from './components/announce/announce-edit.component';
import {AnnounceViewComponent} from './components/announce/announce-view.component';
import {AnnounceListComponent} from './components/announce/announce-list.component';
import {RoleListComponent} from './components/roles/role-list.component';
import {RolePrivilegesComponent} from './components/roles/role-privileges.component';
import {MemberListComponent} from './components/members/member-list.component';
import {SettingsArbitratorComponent} from './components/settings/arbitrator.component';

export const routes: Routes  = [
  { path: '',  redirectTo: 'login', pathMatch:'full' },   
  { path: 'login',  component: LoginComponent },
  { path: 'manage-role-list', component: RoleListComponent},
  { path: 'manage-role-privileges/:id', component: RolePrivilegesComponent},
  { path: 'manage-member-list', component: MemberListComponent},
  { path: 'settings-arbitrator', component: SettingsArbitratorComponent},
  { path: 'function-announce', component: AnnounceTabComponent, children:[
        { path: '', redirectTo: 'list', pathMatch: 'full' },
        { path: 'edit', component: AnnounceEditComponent },
        { path: 'list', component: AnnounceListComponent },
        { path: 'view/:id', component: AnnounceViewComponent }
   ]},
   { path: '**', component: LoginComponent }
  ];

export const appRouterProviders = RouterModule.forRoot(routes);