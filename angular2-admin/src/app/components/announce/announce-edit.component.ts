import {Component} from '@angular/core';

@Component({
    selector: 'announce-edit',
    template: `
         <form action="">
            <table cellpadding="0" cellspacing="0" class="formtab" style="width: 100%;">
                <tr>
                    <th width="50">标题：</th>
                    <td>
                        <input type="text" class="itxt w300 lh36" placeholder="请输入公告标题">
                    </td>
                </tr>
                <tr>
                    <th style="vertical-align: top;">内容：</th>
                    <td>
                        <textarea name="" id="" class="itxt lh36" placeholder="请输入公告内容" style="height: 260px; width: 80%;"></textarea>
                    </td>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <td>
                        <input type="button" value="发布" class="btn w110">
                    </td>
                </tr>
            </table>
        </form>
    `
})
export class AnnounceEditComponent{
    
}