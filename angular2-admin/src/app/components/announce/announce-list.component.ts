import {Component} from '@angular/core';

@Component({
    selector: 'announce-tab',
    template: `
	    <!--公告列表-->
        <div class="announcelist">
            <ul>
                <li class="clearfix">
                    <a href="details.html">
                        <img src="{{defaultImg}}" title="不清楚是否要找律师解决法律问题">
                    </a>
                    <div>
                        <h5>
                            <a href="javascript:;" class="delete"></a>
                            <a href="details.html" class="list-title">不清楚是否要找律师解决法律问题</a>
                        </h5>
                        <p>
                            无论大家最后是否请律师打官司，咨询律师是十分必要的。当事人可能是第一次遇到相关问题，但律师对于当事人所遇的事可谓熟门熟路。毕竟律师已经花费大量的时间与精力在该领域，也接触过类似的当事人，经验比较丰富。无论大家最后是否请律师打官司，咨询律师是十分必要的。当事人可能是第一次遇到相关问题，但律师对于当事人所遇的事可谓熟门熟路。毕竟律师已经花费大量的时间与精力在该领域，也接触过类似的当事人，经验比较丰富....
                        </p>
                    </div>
                </li>
                <li class="clearfix">
                    <a href="details.html">
                        <img src="{{defaultImg}}" title="不清楚是否要找律师解决法律问题">
                    </a>
                    <div>
                        <h5>
                            <a href="javascript:;" class="delete"></a>
                            <a href="details.html" class="list-title">不清楚是否要找律师解决法律问题</a>
                        </h5>
                        <p>
                            无论大家最后是否请律师打官司，咨询律师是十分必要的。当事人可能是第一次遇到相关问题，但律师对于当事人所遇的事可谓熟门熟路。毕竟律师已经花费大量的时间与精力在该领域，也接触过类似的当事人，经验比较丰富。无论大家最后是否请律师打官司，咨询律师是十分必要的。当事人可能是第一次遇到相关问题，但律师对于当事人所遇的事可谓熟门熟路。毕竟律师已经花费大量的时间与精力在该领域，也接触过类似的当事人，经验比较丰富....
                        </p>
                    </div>
                </li>
                <li class="clearfix">
                    <a href="details.html">
                        <img src="{{defaultImg}}" title="不清楚是否要找律师解决法律问题">
                    </a>
                    <div>
                        <h5>
                            <a href="javascript:;" class="delete"></a>
                            <a href="details.html" class="list-title">不清楚是否要找律师解决法律问题</a>
                        </h5>
                        <p>
                            无论大家最后是否请律师打官司，咨询律师是十分必要的。当事人可能是第一次遇到相关问题，但律师对于当事人所遇的事可谓熟门熟路。毕竟律师已经花费大量的时间与精力在该领域，也接触过类似的当事人，经验比较丰富。无论大家最后是否请律师打官司，咨询律师是十分必要的。当事人可能是第一次遇到相关问题，但律师对于当事人所遇的事可谓熟门熟路。毕竟律师已经花费大量的时间与精力在该领域，也接触过类似的当事人，经验比较丰富....
                        </p>
                    </div>
                </li>
                <li class="clearfix">
                    <a href="details.html">
                        <img src="{{defaultImg}}" title="不清楚是否要找律师解决法律问题">
                    </a>
                    <div>
                        <h5>
                            <a href="javascript:;" class="delete"></a>
                            <a href="details.html" class="list-title">不清楚是否要找律师解决法律问题</a>
                        </h5>
                        <p>
                            无论大家最后是否请律师打官司，咨询律师是十分必要的。当事人可能是第一次遇到相关问题，但律师对于当事人所遇的事可谓熟门熟路。毕竟律师已经花费大量的时间与精力在该领域，也接触过类似的当事人，经验比较丰富。无论大家最后是否请律师打官司，咨询律师是十分必要的。当事人可能是第一次遇到相关问题，但律师对于当事人所遇的事可谓熟门熟路。毕竟律师已经花费大量的时间与精力在该领域，也接触过类似的当事人，经验比较丰富....
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    `
})
export class AnnounceListComponent{
	defaultImg = require('../../../assets/images/pic01.png');
}