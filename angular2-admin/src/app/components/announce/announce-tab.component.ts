import {Component} from '@angular/core';

@Component({
    selector: 'announce',
    template: `
        <div class="container">
            <div class="tabbox clearfix">
                <a [routerLink]="['edit']">发布公告</a>
                <a [routerLink]="['list']" class="active">公告列表</a>
            </div>
            <router-outlet></router-outlet> 
        </div>
    `
})
export class AnnounceTabComponent{
    
}