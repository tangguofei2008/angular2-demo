import {Component, OnInit, EventEmitter, Output, Input} from '@angular/core';

@Component({
    selector: 'common-dialog',
    template: `
        <section *ngIf="show">
            <!--遮罩-->
            <div class="mask" style="display: block;"></div>
            <!--信息弹窗-->
	        <div class="popup" style="display: block; top:150px; left:500px;">
		        <div class="popup_tit">
		            <a href="javascript:;" class="close" (click)="_cancel()"></a>
		            <ng-content select="[title]"></ng-content>
		        </div>
		        <div class="prompttxt">
		              <ng-content select="[body]"></ng-content>
		        </div>
		        <div class="listbtn">
		            <input type="button" class="btn" value="确定" (click)="_submit()">
		            <input type="button" class="btn bgc999" value="取消" (click)="_cancel()">
		        </div>
		    </div>
        </section>
    `
})
export class DialogComponent{
    constructor() { }
    
    /**
     * show 是否显示
     * openChange 改变外部的 open
     * next 确认(true)，或取消(false)的 callback
     */
    @Input() show:boolean = false;
    @Output() showChange = new EventEmitter<boolean>();
    @Output() submit = new EventEmitter();
    
    _cancel(){
        this.showChange.emit(false);
    }
    
    _submit(){
    	this.submit.emit();
    	this.showChange.emit(false);
    }
    
  
}

