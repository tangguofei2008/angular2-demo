import {Component} from '@angular/core';
import { Router, RouterLink, ROUTER_DIRECTIVES, ActivatedRoute } from '@angular/router';

import {AuthService} from '../service/auth.service';
import {User} from '../model/user.model';

@Component({
    selector: 'index',
    directives: [RouterLink,  ROUTER_DIRECTIVES],
    providers: [AuthService],
    styles: [`
        .topHide{
		    display:none;
		}
    `],
    template: `
        <!--top 头部-->
	    <div id="top">
	        <div class="top-right fr">
	                                            欢迎您，上城区人民法院！
	            <a href="javascript:;" (click)="exit()">退出</a>
	        </div>
	        <div class="topnav">
	            <a [routerLink]="['function-announce']" [ngClass]="{on: path.indexOf('announce') > -1, topHide: isTopHide('function')}" >焦点公告</a>
	            <a [routerLink]="['manage-role-list']" [ngClass]="{on: path.indexOf('role') > -1, topHide: isTopHide('function')}" >仲裁指南</a>
	            
	            <a [routerLink]="['manage-role-list']" [ngClass]="{on: path.indexOf('role') > -1, topHide: isTopHide('manage')}" >角色</a>
                <a [routerLink]="['manage-member-list']" [ngClass]="{on: path.indexOf('member') > -1, topHide: isTopHide('manage')}" >成员</a>
                
                <a [routerLink]="['settings-arbitrator']" [ngClass]="{on: path.indexOf('arbitrator') > -1, topHide: isTopHide('settings')}" >仲裁管理员</a>
	        </div>
	    </div>
	    <!--left 左导航-->
        <div id="left">
            <a href="javascript:;">
                <div class="logo">
                    <img src="{{logoImg}}" alt="logo" style="margin: 15px 0 0 14px;">
                </div>
            </a>
            <div class="leftnav">
                <a [routerLink]="['function-announce']"  [ngClass]="{current: path.indexOf('function') > -1}">
                    <i class="nav-function"></i><br>功能
                </a>
                <a [routerLink]="['manage-role-list']"  [ngClass]="{current: path.indexOf('manage') > -1}">
                    <i class="nav-function"></i><br>管理
                </a>
                <a href="javascript:;">
                    <i class="nav-statistics"></i><br>统计
                </a>
                <a [routerLink]="['settings-arbitrator']"  [ngClass]="{current: path.indexOf('settings') > -1}">
                    <i class="nav-function"></i><br>设置
                </a>
            </div>
        </div>
	    <!--right 主页面-->
	    <div id="right">
	       <router-outlet (activate)="onActivate($event)"></router-outlet> 
	    </div>
    `
})
export class IndexComponent {
    logoImg = require('../../assets/images/logo.png');

    path: string = '';
    
    user: User;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private authServcie: AuthService) {
    	
    	
        router.events.subscribe((data) => {
            this.path = data.url.substr(1);
        });
    }

    onActivate(event: any){
  
    	this.user = this.authServcie.getUser();
    	
    	console.log(this.user);
    	
    	this.authServcie.isAuthenticated(this.router.url);
    }
    
    //判断二级菜单是否显示
    isTopHide(key: string) {
        return this.path.indexOf(key);
    }

    exit() {
        this.authServcie.removeUser();
    	this.router.navigate(['/login']);
    }
}
