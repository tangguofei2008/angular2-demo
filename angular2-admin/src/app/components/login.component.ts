import {Component} from '@angular/core';
import { Router } from '@angular/router';

import {AuthService} from '../service/auth.service';
import {User} from '../model/user.model';
import {LoginService} from '../service/login.service';

@Component({
    selector: 'login',
    providers: [LoginService],
    template: `
        <div class="loginbg"></div> 
	    <div class="login-font"></div>
	    <div class="login">
	        <div class="login-border"></div>
	        <div class="login-box">
	            <div class="login-title"></div>
	            <form >
	                <div class="loginform" >
	                    <div class="logindiv mtop25">
	                        <i class="user"></i>
	                        <input type="text" class="logintxt" [(ngModel)]="user.username" name="username" placeholder="请输入登录名或手机号">
	                        <span class="placeholder">请输入登录名或手机号</span>
	                    </div>
	                    <div class="logindiv mtop25">
	                        <i class="pwd"></i>
	                        <input type="text" class="logintxt" [(ngModel)]="user.password" name="password" placeholder="请输入登录密码">
	                        <span class="placeholder">请输入登录密码</span>
	                    </div>
	                    <div class="logindiv clearfix">
	                        <a href="find-password-1.html" class="fr forgetpwd">忘记密码</a>
	                    </div>
	                    <input type="button" class="btn" (click)="login()" value="登 录">
	                    <div class="error" *ngIf="!isLoginSuccess">
	                        <i class="ico"></i>登录失败，请检查您的成员名或密码是否填写正确
	                    </div>
	                </div>
	            </form>
	        </div>
	    </div>
    `,
    styleUrls: ['./login.css']
})
export class LoginComponent {
	
    private isLoginSuccess : boolean = true;	

    private user: User = new User('username', 'password');

    constructor(
        private router: Router,
        private loginServcie : LoginService,
        private authService : AuthService) {
    	
    	if(this.authService.getUser() != null){
    		this.router.navigate(['/function-announce/list']);
    	}
    }
    
    login() {
    	console.log(this.user);
    	if(!this.loginServcie.login(this.user.username, this.user.password)){
    		this.isLoginSuccess = false;
    		return false;
    	}
    	this.isLoginSuccess = true;
    	this.authService.setUser(this.user);
        this.router.navigate(['/function-announce/list']);
    }

}
