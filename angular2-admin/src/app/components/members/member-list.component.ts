import {Component} from '@angular/core';

import {PageComponent} from '../../components/common/page.component';

@Component({
    selector: 'member-list',
    directives: [PageComponent],
    template: `
	   <div class="container">
        <div class="tabbox clearfix">
            <a href="javascript:;" class="active">成员</a>
        </div>
        <div class="category">
            <a href="javascript:;" class="btn fr">增加成员</a>
            <select class="select">
                <option value="">全部</option>
                <option value="">仲裁委</option>
                <option value="">秘书办案</option>
            </select>
        </div>

        <div class="listbox">
            <table cellpadding="0" cellspacing="0" class="listtable">
                <tr>
                    <th>编号</th>
                    <th>姓名</th>
                    <th>账号</th>
                    <th>角色</th>
                    <th>加入日期</th>
                    <th>访问次数</th>
                    <th>最后登录</th>
                    <th>最后IP</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>李四</td>
                    <td>15858252236</td>
                    <td>办案秘书</td>
                    <td>
                        2016-07-17<br>
                        14:45:23
                    </td>
                    <td>23</td>
                    <td>
                        2016-07-17<br>
                        14:45:23
                    </td>
                    <td>192.168.0.11</td>
                    <td>正常</td>
                    <td class="operate">
                        <a href="javascript:;">编辑</a>
                        <a href="javascript:;">禁用</a>
                        <a href="javascript:;">删除</a>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>李四</td>
                    <td>15858252236</td>
                    <td>办案秘书</td>
                    <td>
                        2016-07-17<br>
                        14:45:23
                    </td>
                    <td>23</td>
                    <td>
                        2016-07-17<br>
                        14:45:23
                    </td>
                    <td>192.168.0.11</td>
                    <td>禁用</td>
                    <td class="operate">
                        <a href="javascript:;">编辑</a>
                        <a href="javascript:;">启用</a>
                        <a href="javascript:;">删除</a>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>李四</td>
                    <td>15858252236</td>
                    <td>仲裁员</td>
                    <td>
                        2016-07-17<br>
                        14:45:23
                    </td>
                    <td>23</td>
                    <td>
                        2016-07-17<br>
                        14:45:23
                    </td>
                    <td>192.168.0.11</td>
                    <td>正常</td>
                    <td class="operate">
                        <a href="javascript:;">编辑</a>
                        <a href="javascript:;">禁用</a>
                        <a href="javascript:;">删除</a>
                    </td>
                </tr>
            </table>
        </div>
        <!--分页-->
        <common-page 
            [(pagesize)]="pagesize" 
            [total]="total" 
            [current]="current" 
            (pageChange)="pageChange($event)">
        </common-page>
    </div>
	`
})
export class MemberListComponent {
	current = 3;
    total = 35;
    pagesize = 10;
    
    pageChange(event: any){
    	console.log('触发了分页事件:'+event);
    }
}