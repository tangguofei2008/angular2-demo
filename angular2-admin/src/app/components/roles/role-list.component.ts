import {Component} from '@angular/core';
import { Router, RouterLink, ROUTER_DIRECTIVES, ActivatedRoute } from '@angular/router';

import {PageComponent} from '../../components/common/page.component';
import {DialogComponent} from '../../components/common/dialog.component';

import {Role} from '../../model/role.model';

@Component({
    selector: 'role-list',
    directives: [PageComponent, DialogComponent],
    template: `
       <div class="container">
        <div class="tabbox clearfix">
            <a href="javascript:;" class="active">角色列表</a>
        </div>
        <div class="category">
            <a href="javascript:;" class="btn fr" (click)="addRole()">增加角色</a>
            <select class="select">
                <option value="">青岛仲裁委</option>
                <option value="">广州仲裁委</option>
                <option value="">山东仲裁委</option>
            </select>
        </div>

        <div class="listbox">
            <table cellpadding="0" cellspacing="0" class="listtable">
                <tr>
                    <th>编号</th>
                    <th>角色名称</th>
                    <th>角色描述</th>
                    <th>成员人数</th>
                    <th>操作</th>
                </tr>
                
                <tr *ngFor="let role of roles">
                    <td>{{role.id}}</td>
                    <td>{{role.roleName}}</td>
                    <td>{{role.roleDesc}}</td>
                    <td>{{role.members}}</td>
                    <td class="operate">
                        <a href="javascript:;">成员</a>
                        <a href="javascript:;" (click)="toRolePrivileges(role)">权限</a>
                        <a href="javascript:;">编辑</a>
                        <a href="javascript:;">删除</a>
                    </td>
                </tr>
            </table>
        </div>
        
       <!--分页-->
        <common-page 
            [(pagesize)]="pagesize" 
            [total]="total" 
            [current]="current" 
            (pageChange)="pageChange($event)">
        </common-page>
        
    </div>
    
    <common-dialog [(show)]="show" >
        <span title>角色信息</span>
        <div body>
            <table cellspacing="0" cellpadding="0" class="formtab" style="margin-top: 20px;">
	            <tr>
	                <th>角色名称：</th>
	                <td>
	                    <input type="text" class="itxt w260 lh36" />
	                </td>
	            </tr>
	            <tr>
	                <th>角色描述：</th>
	                <td>
	                    <textarea class="itxt lh36" style="width: 248px; height: 120px;"></textarea>
	                </td>
	            </tr>
	        </table>
        </div>
    </common-dialog>
    `
})
export class RoleListComponent{
    private show: boolean = false;

	current = 1;
	total = 35;
	pagesize = 10;

    roles : Role[] = [
         {id:1, roleName:'办案秘书', roleDesc:'案件受理权限', members: 1},
         {id:2, roleName:'仲裁员', roleDesc:'案件受理权限', members: 154},
         {id:3, roleName:'主任', roleDesc:'裁决书盖章', members: 12},
         {id:4, roleName:'办案秘书', roleDesc:'案件受理权限', members: 12},
         {id:5, roleName:'办案秘书', roleDesc:'案件受理权限', members: 1},
         {id:6, roleName:'办案秘书', roleDesc:'案件受理权限', members: 1},
         {id:7, roleName:'仲裁员', roleDesc:'案件受理权限', members: 154},
         {id:8, roleName:'主任', roleDesc:'裁决书盖章', members: 12},
         {id:9, roleName:'办案秘书', roleDesc:'案件受理权限', members: 12},
         {id:10, roleName:'办案秘书', roleDesc:'案件受理权限', members: 1},
         {id:11, roleName:'办案秘书', roleDesc:'案件受理权限', members: 1},
         {id:12, roleName:'仲裁员', roleDesc:'案件受理权限', members: 154},
         {id:13, roleName:'主任', roleDesc:'裁决书盖章', members: 12},
         {id:14, roleName:'办案秘书', roleDesc:'案件受理权限', members: 12},
         {id:15, roleName:'办案秘书', roleDesc:'案件受理权限', members: 1}
    ];

	constructor(private router: Router){
		this.total = this.roles.length;
	}
	
	toRolePrivileges(role: Role){
		this.router.navigate(['/manage-role-privileges', role.id])
	}
    
    
    addRole(){
    	this.show = true;
    }
    
    pageChange(event: any){
    	console.log('pagesize:'+this.pagesize);
        console.log('触发了分页事件:'+event);
    }
}