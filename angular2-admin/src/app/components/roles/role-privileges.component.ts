import {Component} from '@angular/core';
import { Router, ActivatedRoute, Params  } from '@angular/router';

@Component({
    selector: 'role-privileges',
    template: `
		<div class="container">
	        <div class="tabbox clearfix">
	            <a href="role-list.html" class="active">角色权限</a>
	        </div>
	        
	        <div class="category">
	            角色：
	            <select class="select" style="margin-left: 8px;">
	                <option value="">全部</option>
	                <option value="">财务</option>
	                <option value="">办案秘书</option>
	            </select>
	        </div>
	
	        <div class="listintro">
	            给成员***分配权限
	            <label style="margin-left: 15px;">
	                <input type="checkbox" name="" value="">
	                全选
	            </label>
	        </div>
	
	        <div class="listbox">
	            <table cellpadding="0" cellspacing="0" class="listtable">
	                <tr>
	                    <th>业务流程</th>
	                    <th>操作事件</th>
	                </tr>
	                <tr>
	                    <td>
	                        <label>
	                            <input type="checkbox" name="" value="">
	                            待立案
	                        </label>
	                    </td>
	                    <td>
	                        <label>
	                            <input type="checkbox" name="" value="">
	                            立案
	                        </label>
	                    </td>
	                </tr>
	                <tr>
	                    <td>
	                        <label>
	                            <input type="checkbox" name="" value="">
	                            已立案
	                        </label>
	                    </td>
	                    <td>
	                        <label>
	                            <input type="checkbox" name="" value="">
	                            未交费撤诉处理（原告未交费）
	                        </label>
	                        <label>
	                            <input type="checkbox" name="" value="">
	                            撤诉处理（原告撤诉申请）
	                        </label>
	                        <label>
	                            <input type="checkbox" name="" value="">
	                            被告送达处理
	                        </label>
	                    </td>
	                </tr>
	                <tr>
	                    <td>
	                        <label>
	                            <input type="checkbox" name="" value="">
	                            待立案
	                        </label>
	                    </td>
	                    <td>
	                        <label>
	                            <input type="checkbox" name="" value="">
	                            立案
	                        </label>
	                    </td>
	                </tr>
	                <tr>
	                    <td>
	                        <label>
	                            <input type="checkbox" name="" value="">
	                            已立案
	                        </label>
	                    </td>
	                    <td>
	                        <label>
	                            <input type="checkbox" name="" value="">
	                            未交费撤诉处理（原告未交费）
	                        </label>
	                        <label>
	                            <input type="checkbox" name="" value="">
	                            撤诉处理（原告撤诉申请）
	                        </label>
	                        <label>
	                            <input type="checkbox" name="" value="">
	                            被告送达处理
	                        </label>
	                    </td>
	                </tr>
	            </table>
	
	            <div class="listbtn">
	                <input type="button" value="提交" class="btn">
	                <input type="button" value="取消" (click)="goBack()" class="btn bgc999">
	            </div>
	        </div>
	        
	    </div>
	`
})
export class RolePrivilegesComponent {

    constructor(private router: ActivatedRoute) {
        this.router.params.forEach((params: Params) => {
            let id = +params['id'];
            console.log('role id is:' + id);
        });
    }

    goBack() {
        window.history.back();
    }

}