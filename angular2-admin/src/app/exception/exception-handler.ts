import {ExceptionHandler} from '@angular/core';

export class CustomExceptionHandler extends ExceptionHandler{
	call(exception: any, stackTrace?: any, reason?: string) : void{
		alert(exception);
	}
}