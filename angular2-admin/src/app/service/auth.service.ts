import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

import {User} from '../model/user.model';

@Injectable()
export class AuthService {
	
	constructor(private router: Router) {}
	
    user: User;

    /**
     * 不需要登录的url
     */
    uncheckurls : string[] = ['/login', '/register'];

    /**
     * 登陆url
     */
    loginurl : string = '/login';

    /**
     * 检查是否有权限登录
     */
    isAuthenticated(url : String){
    	
    	/**
    	 * 过滤不需要登陆的urls
    	 */
    	for(var value of this.uncheckurls){
    		if(url.startsWith(value)){
    			return true;
    		}
    	}
    	
        if(this.user != null){
            return true;
        }
        
        this.router.navigate([this.loginurl]);
    }
    
    getUser(){
        return this.user;
    }
    
    setUser(inUser: User){
        this.user = inUser;
    }
    
    removeUser(){
    	this.user = null;
    }
}