import { RouterConfig, provideRouter } from '@angular/router';
import {Home } from './components/home';
import {Demo } from './components/demo/demo';
import {ControlForm } from './components/form-controls/control-form';
import {MyForm } from './components/form-controls/my-form';
import {Form2 } from './components/form-controls/form2';


export const routes: RouterConfig = [
  { path: '',  component: Home },    
  { path: 'demo',  component: Demo },
  { path: 'control-form', component: ControlForm},
  { path: 'form2', component: Form2},
  { path: 'my-form', component: MyForm}
];

export const appRouterProviders = [provideRouter(routes)];