import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
 selector: 'demo-comp',
 template: `
  <h1 [ngStyle]="{
    'padding': padding,
    'color': textcolor,
    'font-size': size,
    'background-color': bgcolor
  }">
     {{msg}}
   </h1>
   <label>Message: <input type="text" [value]="msg" (change)="changeMsg($event)"></label>
 `
})
export default class DemoComponent {
  @Input() msg: string;
  @Output() newMsg: EventEmitter<string> = new EventEmitter<string>();

  size: string = '50px';
  bgcolor: string = 'white';
  padding: string = '10px';
  textcolor: string = 'slategrey';

  constructor( ) {}

  changeMsg($event: any) {
    this.msg = $event.target.value;
    this.newMsg.emit(this.msg);
  }

}
