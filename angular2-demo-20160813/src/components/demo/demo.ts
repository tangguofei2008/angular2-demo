import {Component} from '@angular/core';
import DemoComponent from './demo-component';

@Component({
  selector: 'demo',
  directives: [DemoComponent],
  template: `
  <div>
    <demo-comp
      [msg]='"input data"'
      (newMsg)='doStuff($event)'>
    </demo-comp>
  </div>
  `
})
export class Demo {
  doStuff($event) {
    console.log($event);
  }
}
