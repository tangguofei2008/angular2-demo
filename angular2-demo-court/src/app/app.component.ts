import { Component } from '@angular/core';
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router';
import {HTTP_PROVIDERS} from '@angular/http';

import {FootComponent} from './components/common/foot.component';
import {HeaderComponent} from './components/common/header.component';

@Component({
    selector: 'my-app',
    styles: [
             require('../assets/css/base.css'),
             require('../assets/css/style.css')
            ],
    directives: [RouterLink, ROUTER_DIRECTIVES, FootComponent, HeaderComponent],
    template: `
         <div class="top">
	        <div class="w1200">
	            <a href="javascript:;">登录</a>&nbsp;&nbsp;|&nbsp;
	            <a href="javascript:;">注册</a>&nbsp;&nbsp;|&nbsp;
	            <a href="javascript:;">我的诉讼</a>
	        </div>
	    </div>
	    <court-header></court-header>
	    <router-outlet></router-outlet>
	    <court-foot></court-foot>
      `
})
export class AppComponent {
    public path: string = '';

    constructor() {

    }

}


