import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';

import { appRouterProviders} from './app.routes';
import { AppComponent } from './app.component';
@NgModule({
  imports: [
    BrowserModule,
    appRouterProviders
  ],
  declarations: [
    AppComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
