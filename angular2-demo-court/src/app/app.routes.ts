import { Routes, RouterModule } from '@angular/router';

import { HomeComponent} from './components/home.component';
import { OnlineCourtComponent} from './components/home/online-court.component';

export const routes: Routes  = [
  { path: '',  component: HomeComponent },   
  { path: 'home',  component: HomeComponent },   
  { path: 'online-court', component: OnlineCourtComponent }
];

export const appRouterProviders = RouterModule.forRoot(routes);