import {Component} from '@angular/core';

@Component({
	selector: 'court-foot',
	styles: [require('../../../assets/css/base.css'),require('../../../assets/css/style.css')],
	template: `
	   <!--footer-->
	    <div id="footer">
	        <div class="w1200">
	            <div style="border-bottom: #585959 1px solid;">
	                <a href="##">最高人民法院</a>|
	                <a href="##">全国高级人民法院</a>|
	                <a href="##">中国法院网</a>|
	                <a href="##">中国裁判文书网</a>|
	                <a href="##">中国法院庭审直播网</a>|
	                <a href="##">人民法院报</a>|
	                <a href="##">法制日报</a>|
	                <a href="##">北大法律信息网</a>|
	                <a href="##">国家法官学院</a>
	            </div>
	            <div>
	                <a href="##">网站介绍</a>
	                <a href="##">网上声明</a>
	                <a href="##">新手指南</a>
	                <a href="##">联系我们</a>
	                <span>
	                    主办单位：浙江省高级人民法院&nbsp;&nbsp;中文域名：浙江法院电子商务网上法庭
	                </span>
	            </div>
	        </div>
	    </div>
	`
})
export class FootComponent{
	
}