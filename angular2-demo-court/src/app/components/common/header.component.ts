import {Component} from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router'


@Component({
    selector: 'court-header',
    styles: [require('../../../assets/css/base.css'),require('../../../assets/css/style.css')],
    template: `
	   <!--header-->
	    <div class="header">
	        <div class="w1200 clearfix">
	            <div class="h_right">
	                <a href="javascript:;">我是原告</a>
	                <a href="javascript:;">我是被告</a>
	            </div>
	            <a href="index.html" class="logo">
	                <img src="{{logo}}" alt="logo">浙江金融网上法庭
	            </a>
	            <!--菜单-->
	            <div class="menu">
	                <a routerLink="/home" routerLinkActive="active" [ngClass]="{current: path=='home'}">
	                    <span>首&nbsp;页</span><i></i>
	                </a>
	                <a routerLink="/online-court" routerLinkActive="active" [ngClass]="{current: path=='online-court'}">
	                    <span>在线法院</span><i></i>
	                </a>
	                <a href="javascript:;" [ngClass]="{current: path=='case_seach'}">
	                    <span>案件查询</span><i></i>
	                </a>
	                <a href="javascript:;" [ngClass]="{current: path=='txfw'}">
	                    <span>调解服务</span><i></i>
	                </a>
	                <a href="javascript:;" [ngClass]="{current: path=='help'}">
	                    <span>帮助中心</span><i></i>
	                </a>
	            </div>
	        </div>
	    </div>
	`
})
export class HeaderComponent {
    logo = require('../../../assets/img/index_logo.png');
    path : string;
    
    constructor(
    		private router: Router,
    		private activatedRoute: ActivatedRoute){
    	router.events.subscribe((data) => {
            this.path = data.url.substr(1);
            if(this.path == ''){
            	this.path = 'home';
            }
    	});
    }
    
    
}