import {Component} from '@angular/core';

@Component({
	selector: '',
	styles: [
             require('../../assets/css/base.css'),
             require('../../assets/css/common.css'),
             require('../../assets/css/mysuit.css')
            ],
	template: `
	   <div class="fn-BGC-FFF fn-color-666  person-center">
    <div class="fn-PL20 fn-PR20  fn-PB50">
        <!-- 身份 -->
        <div class="fn-clear person-center-status">
            <img src="img/headl.png" alt="" class="child-image">
            <div class="child-box">
                <div class="child-info">
    
                 <span class="fn-FS14 fn-FWB fn-color-e50912">已认证</span> <br/>
                 <span class="">证件号码：<span class="card-number">3****************7</span></span><br/>
                 <span class="">注册手机号码：*******0755</span>
    
                </div>

            </div>
        </div>
        <!-- 基本信息 -->
        <div class="global-tab fn-BBD-ebebeb fn-MT20 JS-basic-info-title">
            <i></i>基本信息
            <a href="javascript:;" class="fn-btn fn-btn-default fn-btn-mi fn-ML20 fn-MT5 JS-edit-info fn-MB10 ">编辑信息</a>
        </div>
        <div class="fn-MT20 JS-target-list JS-basic-info" data-widget-cid="widget-3">
    <table class=" fn-table-text fn-ML30">
        <tbody><tr>
            <td >身份类型：个人</td>
            <td align="right" width="90"> </td>
            <td> </td>
        </tr>
        <tr>
            <td >姓名：&nbsp;管骥宇</td>
            <td align="right" width="300"> </td>
            <td >性别：&nbsp;女</td>
        </tr>
        <tr>
            <td >民族：&nbsp;汉</td>
            <td align="right" width="300"> </td>
            <td >国籍：&nbsp;中国</td>
        </tr>
        <tr>
            <td >户籍地址：&nbsp;</td>
            <td></td>
            <td >当前住址：&nbsp;文一西路1</td>
        </tr>
        <tr>
            <td >邮寄送达地址:&nbsp;文一西路</td>
            <td align="right" width="300"> </td>
            <td >手机号码：&nbsp;*******9507</td>
        </tr>
        <tr>
            <td >邮箱：&nbsp;249***@qq.com</td>
            <td align="right" width="300"> </td>
            <td >固定电话：&nbsp;0571-*****556</td>
            <td></td>
        </tr>
        <tr>
            <td >职业：</td>
            <td></td>
        </tr>
    </tbody></table>
</div>

        <!-- 验证信息 -->
        <div class="JS-validate-info">
    <div class="global-tab fn-BBD-ebebeb fn-MT20">
        <i></i>身份验证信息
        
            <input type="button" class="fn-btn fn-btn-default fn-ML20 fn-btn-mi JS-certificate fn-MB10" value="重新认证">
        
    </div>
    <div class="fn-MT20 JS-target-list ">
  
             <table class=" fn-table-text">
                <tbody><tr>
                    <td align="right" width="90">认证类别：</td>
                    <td>
                    
                    支付宝
                    </td>
                </tr>
                <tr>
                    <td align="right" width="90">认证名称：</td>
                    <td>孙玲玲</td>
                </tr>
                <tr>
                    <td align="right">证件类型：</td>
                    <td>身份证</td>
                </tr>
                <tr>
                    <td align="right">证件号码：</td>
                    <td>3****************7</td>
                </tr>
            </tbody>
            </table>
  
        </div>
      </div>

    </div>
</div>
	`
})
export class HomeComponent{
	
}