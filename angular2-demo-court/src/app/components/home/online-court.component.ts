import {Component, OnInit} from '@angular/core';

import {OnlineCourt} from './online-court';
import {OnlineCourtService} from './online-court.service';


@Component({
	selector: 'online-court',
	styles: [
	         require('../../../assets/css/base.css'),
	         require('../../../assets/css/style.css'),
	         require('../../../assets/css/index.css'),
	         require('../../../assets/css/lform.css'),
	         require('../../../assets/css/new.css')
	        ],
	providers: [OnlineCourtService],
	template: `
	    <div class="w1200 courlist" >
	       <div class="list clearfix" *ngFor="let court of courts">
	           <img src="{{defaultPhoto}}"/>
	           <div class="content">
                    <h1>{{court.name}}</h1>
                    <p>{{court.desc}}</p>
               </div>
	       </div>
	    </div>
	`
})

export class OnlineCourtComponent implements OnInit{
    defaultPhoto = require('../../../assets/img/caseb.jpg');
	courts: OnlineCourt[];
    
    constructor(
    		private courtServcie : OnlineCourtService){
    	
    }
    
    ngOnInit() {
    	 this.courtServcie.getCourts().then(courts => this.courts= courts);
    }
}


