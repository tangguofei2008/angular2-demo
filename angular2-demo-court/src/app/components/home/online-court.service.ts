import {Injectable} from '@angular/core';

import {OnlineCourt} from './online-court';

export const ONLINE_COURTS : OnlineCourt[] = [
		{name:'杭州上城区人民法院', desc:'受理互联网金融纠纷网上法庭试点基层法院审结的上诉案件，指导区级人民法院试点工作'},
		{name:'杭州市低级人民法院', desc:'受理互联网金融纠纷网上法庭试点基层法院审结的上诉案件，指导区级人民法院试点工作'},
		{name:'宁波市高级人民法院', desc:'受理互联网金融纠纷网上法庭试点基层法院审结的上诉案件，指导区级人民法院试点工作'},
		{name:'宁波市中级人民法院', desc:'受理互联网金融纠纷网上法庭试点基层法院审结的上诉案件，指导区级人民法院试点工作'}
];


@Injectable()
export class OnlineCourtService{
    getCourts(){
    	 return Promise.resolve(ONLINE_COURTS);
    }	
}