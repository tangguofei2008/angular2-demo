import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import {HTTP_PROVIDERS} from '@angular/http';

import {InputText,DataTable,Button,Dialog,Column,Header,Footer} from 'primeng/primeng';

import { HeroService } from './services/hero.service';
import { CarService } from './services/car.service';
import { Car } from './models/car';
import { FootComponent } from './components/foot.component';


@Component({
    selector: 'primeng-showcase',
    template: `
        <div class="PC">
            <div id="MENUSIDE" [ngClass]="{'active':mobileMenuActive}">
		        <div id="MENUSIDEindent">
		            <span id="LOGO" class="">
		                <a href="#" [routerLink]="['/']">
		                    <img alt="logo" src="resources/images/primeng.svg" />
		                </a>
		                <a href="#" id="openMobileMenuBtn" (click)="toggleMenu($event)" [ngClass]="{'active':mobileMenuActive}">
		                    <img alt="logo" src="resources/images/menuicon.svg"/>
		                </a>
		            </span>
		
		            <span id="menu_input" #inputmenutitle class="MenuSideMainLink bordersOfMenuSide" [class.MenuSideMainLinkDark]="inputmenutitle.id == activeMenuId" (click)="activeMenuId = inputmenutitle.id"><img src="resources/images/mono/input.svg" /><img class="hiddenIcons" src="resources/images/mono/inputBlue.svg" /><span class="MainLinkText">Input</span></span>
		            <div class="SubMenuLinkContainer" [ngClass]="{submenushow: (inputmenutitle.id == activeMenuId), 'submenuhide': (inputmenutitle.id != activeMenuId)}">
		                <a class="SubMenuLink" [routerLink]="['/users']" (click)="mobileMenuActive = false">&#9679; 用户管理</a>
		            </div>
		
		            <span id="button_menutitle" #buttonmenutitle class="MenuSideMainLink bordersOfMenuSide" [class.MenuSideMainLinkDark]="buttonmenutitle.id == activeMenuId" (click)="activeMenuId = buttonmenutitle.id"><img src="resources/images/mono/button.svg" /><img class="hiddenIcons" src="resources/images/mono/buttonBlue.svg" /><span class="MainLinkText">Button</span></span>
		            <div class="SubMenuLinkContainer" [ngClass]="{'submenushow': (buttonmenutitle.id == activeMenuId), 'submenuhide': (buttonmenutitle.id != activeMenuId)}">
		                <a class="SubMenuLink" [routerLink]="['/dashboard']" (click)="mobileMenuActive = false">&#9679; Button</a>
		                <a class="SubMenuLink" [routerLink]="['/heroes']" (click)="mobileMenuActive = false">&#9679; SplitButton</a>
		            </div>
		
		            <span id="data_menutitle" #datamenutitle class="MenuSideMainLink bordersOfMenuSide" [class.MenuSideMainLinkDark]="datamenutitle.id == activeMenuId" (click)="activeMenuId = datamenutitle.id"><img src="resources/images/mono/data.svg" /><img class="hiddenIcons" src="resources/images/mono/dataBlue.svg" /><span class="MainLinkText">Data</span></span>
		            <div class="SubMenuLinkContainer" [ngClass]="{'submenushow': (datamenutitle.id == activeMenuId), 'submenuhide': (datamenutitle.id != activeMenuId)}">
		                <a class="SubMenuLink" [routerLink]="['/dashboard']" (click)="mobileMenuActive = false">&#9679; Carousel</a>
		                <a class="SubMenuLink" [routerLink]="['/heroes']" (click)="mobileMenuActive = false">&#9679; DataGrid</a>
		            </div>
		
		            <span id="menu_multimedia" #multimediamenutitle class="MenuSideMainLink bordersOfMenuSide MenuSideMainLinkDark" [class.MenuSideMainLinkDark]="multimediamenutitle.id == activeMenuId"
		                  (click)="activeMenuId = multimediamenutitle.id"><img src="resources/images/mono/multimedia.svg">
		                    <img class="hiddenIcons" src="resources/images/mono/multimediaBlue.svg" style="opacity: 0;">
		                    <span class="MainLinkText">Multimedia</span>
		            </span>
		            <div class="SubMenuLinkContainer" [ngClass]="{'submenushow': (multimediamenutitle.id == activeMenuId), 'submenuhide': (multimediamenutitle.id != activeMenuId)}">
		                <a class="SubMenuLink" [routerLink]="['/dashboard']" (click)="mobileMenuActive = false">&#9679; Galleria</a>
		            </div>
		        </div>
            </div>
            
            
            <div id="CONTENTSIDE">
		        <div id="CONTENTSIDEindent">
		            <!-- header bar start-->
		            <div class="ContentSideSections" id="PFTopLinksCover" style="height:30px;">
		                <a id="LOGOTEXTSIDE" [routerLink]="['/']">
		                    <img alt="logo" src="showcase/resources/images/primengfull.svg"/>
		                </a>
		
		                <a href="#" id="openMenuBtn" (click)="toggleMenu($event)" [ngClass]="{'active':mobileMenuActive}">
		                    <img alt="logo" src="showcase/resources/images/menuicon.svg"/>
		                </a>
		
		                <a href="http://forum.primefaces.org/viewforum.php?f=35" class="PFTopLinks floatRight"><img alt="mockosx" src="showcase/resources/images/community.svg" /><span class="PFDarkText">Forum</span></a>
		
		                <span class="PFTopLinks floatRight cursorPointer" id="themeSwitcher" (mouseenter)="themesVisible = true" (mouseleave)="themesVisible = false">
		                    <img src="showcase/resources/images/themeswitcher.svg" /><span class="PFDarkText">Free Themes</span>
		                    <div id="GlobalThemeSwitcher" class="navOverlay" [style.display]="themesVisible ? 'block' : 'none'" style="margin-top:10px">
		                        <a href="#" data-theme="afterdark"  onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-afterdark"></span><span class="ui-text">Afterdark</span></a>
		                        <a href="#" data-theme="afternoon" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-afternoon"></span><span class="ui-text">Afternoon</span></a>
		                        <a href="#" data-theme="afterwork" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-afterwork"></span><span class="ui-text">Afterwork</span></a>
		                        <a href="#" data-theme="aristo" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-aristo"></span><span class="ui-text">Aristo</span></a>
		                        <a href="#" data-theme="black-tie" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-black-tie"></span><span class="ui-text">Black-Tie</span></a>
		                        <a href="#" data-theme="bluesky" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-bluesky"></span><span class="ui-text">Bluesky</span></a>
		                        <a href="#" data-theme="bootstrap" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-bootstrap"></span><span class="ui-text">Bootstrap</span></a>
		                        <a href="#" data-theme="casablanca" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-casablanca"></span><span class="ui-text">Casablanca</span></a>
		                        <a href="#" data-theme="cruze" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-cruze"></span><span class="ui-text">Cruze</span></a>
		                        <a href="#" data-theme="dark-hive" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-dark-hive"></span><span class="ui-text">Dark-Hive</span></a>
		                        <a href="#" data-theme="delta" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-delta"></span><span class="ui-text">Delta</span></a>
		                        <a href="#" data-theme="dot-luv" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-dot-luv"></span><span class="ui-text">Dot-Lov</span></a>
		                        <a href="#" data-theme="eggplant" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-eggplant"></span><span class="ui-text">Eggplant</span></a>
		                        <a href="#" data-theme="excite-bike" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-excite-bike"></span><span class="ui-text">Excite-Bike</span></a>
		                        <a href="#" data-theme="flick" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-flick"></span><span class="ui-text">Flick</span></a>
		                        <a href="#" data-theme="glass-x" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-glass-x"></span><span class="ui-text">Glass-X</span></a>
		                        <a href="#" data-theme="home" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-home"></span><span class="ui-text">Home</span></a>
		                        <a href="#" data-theme="hot-sneaks" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-hot-sneaks"></span><span class="ui-text">Hot-Sneaks</span></a>
		                        <a href="#" data-theme="humanity" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-humanity"></span><span class="ui-text">Humanity</span></a>
		                        <a href="#" data-theme="le-frog" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-le-frog"></span><span class="ui-text">Le-Forg</span></a>
		                        <a href="#" data-theme="midnight" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-midnight"></span><span class="ui-text">Midnight</span></a>
		                        <a href="#" data-theme="mint-choc" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-mint-choc"></span><span class="ui-text">Mint-Choc</span></a>
		                        <a href="#" data-theme="omega" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-omega"></span><span class="ui-text">Omega</span></a>
		                        <a href="#" data-theme="overcast" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-overcast"></span><span class="ui-text">Overcast</span></a>
		                        <a href="#" data-theme="pepper-grinder" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-pepper-grinder"></span><span class="ui-text">Pepper-Grinder</span></a>
		                        <a href="#" data-theme="redmond" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-redmond"></span><span class="ui-text">Redmond</span></a>
		                        <a href="#" data-theme="rocket" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-rocket"></span><span class="ui-text">Rocket</span></a>
		                        <a href="#" data-theme="sam" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-sam"></span><span class="ui-text">Sam</span></a>
		                        <a href="#" data-theme="smoothness" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-smoothness"></span><span class="ui-text">Smoothness</span></a>
		                        <a href="#" data-theme="south-street" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-south-street"></span><span class="ui-text">South-Street</span></a>
		                        <a href="#" data-theme="start" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-start"></span><span class="ui-text">Start</span></a>
		                        <a href="#" data-theme="sunny" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-sunny"></span><span class="ui-text">Sunny</span></a>
		                        <a href="#" data-theme="swanky-purse" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-swanky-purse"></span><span class="ui-text">Swanky-Purse</span></a>
		                        <a href="#" data-theme="trontastic" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-trontastic"></span><span class="ui-text">Trontastic</span></a>
		                        <a href="#" data-theme="ui-darkness" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-ui-darkness"></span><span class="ui-text">UI-Darkness</span></a>
		                        <a href="#" data-theme="ui-lightness" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-ui-lightness"></span><span class="ui-text">UI-Lightness</span></a>
		                        <a href="#" data-theme="vader" onclick="DemoApp.changeTheme(event, this)"><span class="ui-theme ui-theme-var"></span><span class="ui-text">Vader</span></a>
		                    </div>
		                </span>
		                
		                <span class="PFTopLinks floatRight cursorPointer" id="PremiumLayouts" (mouseenter)="premiumVisible = true" (mouseleave)="premiumVisible = false">
		                    <img src="showcase/resources/images/layouts.svg" /><span class="PFDarkText">Premium</span>
		                    <div id="PremiumLayoutsPanel" class="navOverlay" [style.display]="premiumVisible ? 'block' : 'none'" style="margin-top:10px">
		                        <a href="http://www.primefaces.org/layouts/rio-primeng" target="_blank"><span class="ui-text">Rio</span></a>
		                        <a href="http://www.primefaces.org/layouts/modena-primeng" target="_blank"><span class="ui-text">Modena</span></a>
		                        <a href="http://www.primefaces.org/layouts/adamantium-primeng" target="_blank"><span class="ui-text">Adamantium</span></a>
		                        <a href="http://www.primefaces.org/layouts/olympos-primeng" target="_blank"><span class="ui-text">Olympos</span></a>
		                    </div>
		                </span>
		
		                <a href="setup.html" class="PFTopLinks floatRight hashed" [routerLink]="['/setup']"><img alt="mockosx" src="showcase/resources/images/setup.svg" /><span class="PFDarkText">Setup</span></a>
		
		                <div class="mobileLogoCover">
		                    <img src="showcase/resources/images/primengdark.svg" style="height:30px;" class="floatLeft marginRight10" />
		                    <span class="dispBlock logoDarkText fontSize18 floatRight" style="margin:-5px 0px 0px 5px;">PrimeNG <span class="dispBlock logoBlueText fontSize14">SHOWCASE</span></span>
		                </div>
		            </div>
		            <!-- header bar end-->
		
		            <div>
		                <router-outlet></router-outlet>
		            </div>
		
		
                    <foot></foot>	
		        </div>
		    </div>
            
            
        </div>
    `,
    directives: [ROUTER_DIRECTIVES, InputText,DataTable,Button,Dialog,Column,Header,Footer, FootComponent],
    providers: [
                HeroService, CarService, HTTP_PROVIDERS
            ]
})
export class AppComponent {

    activeMenuId: string;

    themesVisible: boolean = false;

    mobileMenuActive: boolean = false;

    toggleMenu(e) {
        this.mobileMenuActive = !this.mobileMenuActive;
        e.preventDefault();
    }
}
