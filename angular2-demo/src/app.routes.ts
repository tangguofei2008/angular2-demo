import { RouterConfig, provideRouter } from '@angular/router';

import { DashboardComponent } from './components/dashboard.component';
import { HeroesComponent } from './components/heroes.component';
import { HeroDetailComponent } from './components/hero-detail.component';
import { FootComponent } from './components/foot.component';
import { HomePageComponent } from './homepage.component';
import { UserComponent } from './components/user/user.component';


export const routes: RouterConfig = [
  { path: '',  component: HomePageComponent },                              
  { path: 'dashboard',  component: DashboardComponent },
  { path: 'detail/:id', component: HeroDetailComponent },
  { path: 'heroes',     component: HeroesComponent  },
  { path: 'users',      component: UserComponent},
  { path: 'foot',       component: FootComponent  }
];

export const appRouterProviders = [provideRouter(routes)];