import { Component } from '@angular/core';

@Component({
    selector: 'foot',
	template: `
	   <div id="footer">
        <div class="w1200">
            <div style="border-bottom: #585959 1px solid;">
                <a href="http://www.court.gov.cn/" target="_blank">最高人民法院</a>|
                <a href="http://splcgk.court.gov.cn/zgsplcxxgkw/" target="_blank">中国审判流程信息公开网</a>|
                <a href="http://shixin.court.gov.cn/" target="_blank">中国执行信息公开网</a>|
                <a href="http://wenshu.court.gov.cn/" target="_blank">中国裁判文书网</a>|
                <a href="http://baike.baidu.com/link?url=0jKrz9TXAQn_v0GNIFqPUPYFw_dXtezMLFLswYS2KLhWpxztjvqqZ8LteDr0_r27C6cH-SWcZfNu0FZobC72ca" target="_blank">杭州上城区人民法院</a>|
                <a href="http://www.zjsfgkw.cn/" target="_blank">浙江法院公开网</a>|
                <a href="http://www.zjjcy.gov.cn/" target="_blank">浙江检察网</a>
            </div>
            <div>
                <a href="##">网站介绍</a>
                <a href="##">网上声明</a>
                <a href="##">联系我们</a>
                <span>
                    主办单位：浙江省高级人民法院&nbsp;&nbsp;中文域名：智慧法院
                </span>
            </div>
        </div>
    </div>
	`
})
export class FootComponent {

}