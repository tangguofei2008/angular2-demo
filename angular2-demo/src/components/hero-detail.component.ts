import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute }       from '@angular/router';

import { Hero } from '../models/hero';
import { HeroService } from '../services/hero.service';

@Component({
    selector: 'my-hero-detail',
    template: `
        <div *ngIf="hero">
		  <h2>{{hero.name}} details!</h2>
		  <div>
		    <label>id: </label>{{hero.id}}</div>
		  <div>
		    <label>name: </label>
		    <input [(ngModel)]="hero.name" placeholder="name" />
		  </div>
		  <button (click)="goBack()">Back</button>
		</div>
  `,
    styles: [`
        label {
		  display: inline-block;
		  width: 3em;
		  margin: .5em 0;
		  color: #607D8B;
		  font-weight: bold;
		}
		input {
		  height: 2em;
		  font-size: 1em;
		  padding-left: .4em;
		}
		button {
		  margin-top: 20px;
		  font-family: Arial;
		  background-color: #eee;
		  border: none;
		  padding: 5px 10px;
		  border-radius: 4px;
		  cursor: pointer; cursor: hand;
		}
		button:hover {
		  background-color: #cfd8dc;
		}
		button:disabled {
		  background-color: #eee;
		  color: #ccc; 
		  cursor: auto;
		}
  `]
})
export class HeroDetailComponent implements OnInit, OnDestroy {
    @Input() hero: Hero;

    private sub: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private _heroService: HeroService) {
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            let id = +params['id'];
            this._heroService.getHero(id).then(hero => this.hero = hero);
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    goBack() {
        window.history.back();
    }
}
