import { Component, OnInit } from '@angular/core';
import { Router, ROUTER_DIRECTIVES } from '@angular/router';
import {HTTP_PROVIDERS} from '@angular/http';

import {InputText,DataTable,Button,Dialog,Column,Header,Footer} from 'primeng/primeng';

import {UserService } from '../../services/user.service';
import {User} from '../../models/user';

@Component({
    template: `
        <div class="ContentSideSections">
            <div class="Content100 overHidden TextShadow">
                <span class="fontSize30 TextShadow orange mediumFont marginBottom20 dispBlock">用户管理->基于primeng-ui</span>
                <span class="defaultText dispTable">This sample demo for primeng-ui</span>
            </div>
        </div>
        
        <div class="ui-widget-header ui-helper-clearfix" style="padding:4px 10px;border-bottom: 0 none">
		    <span class="defaultText dispTable">姓名</span>
		    <input #gb type="text" pInputText size="30">
		    
		    <button type="button" pButton icon="fa-plus" style="float:left" label="查询"></button>
		</div>
    
        <div class="ContentSideSections Implementation">
            <p-dataTable [value]="users" 
                    selectionMode="single" 
                    [(selection)]="selectedUser" 
                    (onRowSelect)="onRowSelect($event)" 
                    [paginator]="true" 
                    [rows]="10" 
                    [responsive]="true">
                <p-column field="id" header="ID" [sortable]="true"></p-column>
                <p-column field="phone" header="手机号码" [sortable]="true"></p-column>
                <p-column field="name" header="姓名" [sortable]="true"></p-column>
                <p-column field="sex" header="性别" [sortable]="true"></p-column>
                <p-column field="address" header="住址" [sortable]="true"></p-column>
                <p-column field="mail" header="电子邮件" [sortable]="true"></p-column>
                <footer>
                    <div class="ui-helper-clearfix" style="width:100%">
                           <button type="button" pButton icon="fa-plus" style="float:left" (click)="showDialogToAdd()" label="Add"></button>
                    </div>
                </footer>
            </p-dataTable>
        </div>
    `,
    directives: [ROUTER_DIRECTIVES, InputText, DataTable, Button, Dialog, Column, Header, Footer],
    providers: [
        UserService, HTTP_PROVIDERS
    ]

})
export class UserComponent implements OnInit {

    users: User[];

    selectedUser: User;

    constructor(
        private _router: Router,
        private _userService: UserService) { }

    ngOnInit() {
        this._userService.getUsers().then(users => this.users = users);
    }

}