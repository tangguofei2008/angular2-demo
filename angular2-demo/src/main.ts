import { bootstrap }    from '@angular/platform-browser-dynamic';
import { AppComponent } from './app.component';
import { appRouterProviders } from './app.routes';

import {disableDeprecatedForms, provideForms} from '@angular/forms';
import 'rxjs/Rx';

bootstrap(AppComponent, [
                         disableDeprecatedForms(),
                         provideForms(),
                         appRouterProviders]);