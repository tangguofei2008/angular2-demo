/**
 * 用户Model
 */
export class User {
    //组件
    id: number;
    //手机号码
    phone: String;
    //姓名
    name: String;
    //性别1:男0:女
    sex: number;
    //地址
    address: String;
    //邮箱地址
    mail: String; 
}