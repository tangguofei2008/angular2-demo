import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {User} from '../models/user';

@Injectable()
export class UserService{

	constructor(private http: Http) {}

	/**
	 * 获取用户列表
	 */
    getUsers(){
    	return this.http.get('src/services/user.json')
				        .toPromise()
				        .then(res => <User[]> res.json().data)
				        .then(data => { return data; });
    }

}